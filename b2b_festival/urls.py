"""b2b_festival URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

from b2b_festival import settings

# urlpatterns = [
#     path("", include('main.urls')),
#     path('admin/', admin.site.urls),
#     path("dda_blog/", include('dda_blog.urls')),
# ]
urlpatterns = [
    path("api/", include('dance_school.urls')),
    path('admin/', admin.site.urls),
    path("api/b2b/", include('main.urls')),
    path("api/blog/", include('dda_blog.urls')),
    path(r'^tinymce/', include('tinymce.urls')),
    path(r'^api-auth/', include('rest_framework.urls'))
]
urlpatterns += staticfiles_urlpatterns()
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
