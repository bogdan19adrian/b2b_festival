from rest_framework import serializers

from main.models import ConfirmedPhoto


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConfirmedPhoto
        fields = '__all__'
