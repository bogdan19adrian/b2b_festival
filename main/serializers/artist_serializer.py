from rest_framework import serializers

from main.models import ConfirmedArtists


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConfirmedArtists
        fields = '__all__'
