from rest_framework import serializers

from main.models import ConfirmedDJs


class DjSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConfirmedDJs
        fields = '__all__'
