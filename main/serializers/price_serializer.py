from rest_framework import serializers

from main.models import Pricing


class PricesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pricing
        fields = '__all__'