from rest_framework import serializers

from main.models import  AboutBullet


class AboutBulletSerializer(serializers.ModelSerializer):
    class Meta:
        model = AboutBullet
        fields = ['id', 'about_bullet_title', 'about_bullet_description', 'about_bullet_icon']