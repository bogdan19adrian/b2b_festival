from rest_framework import serializers

from main.models import PaymentOption


class PaymentOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentOption
        fields = '__all__'