from django.urls import path
from . import views


urlpatterns = [
    path('festival/carousel/', views.carousel_main_list),
    path('festival/program/', views.program_main_obj),
    path('festival/about/', views.about_main_obj),
    path('festival/aboutbullet/', views.about_bullet_list),
    path('festival/artists/', views.artist_main_list),
    path('festival/djs/', views.dj_main_list),
    path('festival/photo/', views.photo_main_list),
    path('festival/tickets/', views.buy_ticket_message),
    path('festival/prices/', views.prices_main_list),
    path('festival/payment/', views.payment_main_list),
    path('festival/message/', views.send_b2b_message),


]
