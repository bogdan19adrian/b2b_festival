from django.urls import path
from . import views

urlpatterns = [
    path('posts/', views.blog_main_post_list),
    path('filtered/', views.blog_filtered_post_list),
    path('details/<int:pk>', views.blog_detail),

]
