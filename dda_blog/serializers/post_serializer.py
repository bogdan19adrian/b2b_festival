from rest_framework import serializers

from dda_blog.models import Post
from main.models import About


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'
