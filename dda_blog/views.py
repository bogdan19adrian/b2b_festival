from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Post
from .serializers.post_serializer import PostSerializer


@api_view(['GET'])
def blog_main_post_list(request):
    if request.method == 'GET':
        snippets = Post.objects.filter(status=1).order_by('-created_on')
        serializer = PostSerializer(snippets, many=True)
        if snippets is not None:
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def blog_filtered_post_list(request):
    if request.method == 'GET':
        snippets = Post.objects.filter(status=1, category=1).order_by('-created_on')
        serializer = PostSerializer(snippets, many=True)
        if snippets is not None:
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def blog_detail(request, pk):
    try:
        snippet = Post.objects.get(pk=pk)
    except Post.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if request.method == 'GET':
        snippets = Post.objects.get(pk=pk)
        serializer = PostSerializer(snippets, many=False)
        if snippets is not None:
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
