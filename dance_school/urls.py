from django.urls import path

from dance_school import views

urlpatterns = [
    path('school/carousel/', views.carousel_list),
    path('school/about/', views.about_list),
    path('school/post/', views.post_last),
    path('school/instructor/', views.instructor_list),
    path('school/program/', views.program_list),
    path('school/dayprogram/', views.day_program_list),
    path('school/message/', views.send_school_site_message),
    path('school/team/', views.team_list),
    path('school/contact/', views.contact_obj),

    #
    path("", views.dance_school, name="dance_school"),
]
