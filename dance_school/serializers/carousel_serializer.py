from rest_framework import serializers

from dance_school.models import Carousel


class CarouselSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carousel
        fields = ['carousel_image']
