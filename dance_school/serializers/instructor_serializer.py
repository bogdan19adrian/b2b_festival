from rest_framework import serializers

from dance_school.models import Instructor


class InstructorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instructor
        fields = ['id', 'instructor_name', 'instructor_description', 'instructor_image', 'instructor_published']
