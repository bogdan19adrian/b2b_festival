from rest_framework import serializers

from dance_school.models import About


class AboutSerializer(serializers.ModelSerializer):
    class Meta:
        model = About
        fields = ['id', 'about_title', 'about_description']
