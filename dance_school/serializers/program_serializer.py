from rest_framework import serializers

from dance_school.models import About, ProgramInterval, DayProgram


class ProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProgramInterval
        fields = ['id', 'group_type', 'hour_interval', 'dance_style', 'day_program']


class DayProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = DayProgram
        fields = ['id', 'program_day']
