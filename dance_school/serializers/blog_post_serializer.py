from rest_framework import serializers

from dda_blog.models import Post


class BlogPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'title', 'slug', 'author', 'content']
