from logging import log

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from common.MailSenderWrapper import MailSenderWrapper
from common.common import validate_email
from dance_school.models import Carousel, ProgramInterval, DayProgram, Instructor, Team
from dance_school.serializers.about_serializer import AboutSerializer
from dance_school.serializers.blog_post_serializer import BlogPostSerializer
from dance_school.serializers.carousel_serializer import CarouselSerializer
from dance_school.serializers.contact_serializer import ContactSerializer
from dance_school.serializers.instructor_serializer import InstructorSerializer
from dance_school.serializers.program_serializer import ProgramSerializer, DayProgramSerializer
from dance_school.serializers.team_serializer import TeamSerializer


@api_view(['GET'])
def carousel_list(request):
    if request.method == 'GET':
        from dance_school.models import Carousel
        snippets = Carousel.objects.all()
        serializer = CarouselSerializer(snippets, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def about_list(request):
    if request.method == 'GET':
        from dance_school.models import About
        snippets = About.objects.all()
        serializer = AboutSerializer(snippets, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def post_last(request):
    if request.method == 'GET':
        from dda_blog.models import Post
        snippets = Post.objects.filter(status=1).order_by('-created_on')[0]
        serializer = BlogPostSerializer(snippets, many=False)
        return Response(serializer.data)


@api_view(['GET'])
def instructor_list(request):
    if request.method == 'GET':
        snippets = Instructor.objects.all()
        serializer = InstructorSerializer(snippets, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def team_list(request):
    if request.method == 'GET':
        snippets = Team.objects.all()
        serializer = TeamSerializer(snippets, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def contact_obj(request):
    if request.method == 'GET':
        from dance_school.models import Contact
        snippets = Contact.objects.last(),
        serializer = ContactSerializer(snippets, many=True)
        if snippets is not None:
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def program_list(request):
    if request.method == 'GET':
        snippets = ProgramInterval.objects.all()
        serializer = ProgramSerializer(snippets, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def day_program_list(request):
    if request.method == 'GET':
        snippets = DayProgram.objects.all()
        serializer = DayProgramSerializer(snippets, many=True)
        return Response(serializer.data)


def dance_school(request):
    return render(request=request,
                  template_name=None,
                  context={
                      "carousel": Carousel.objects.order_by('carousel_order'),
                  })


@api_view(['POST'])
@csrf_exempt
def send_school_site_message(request):
    if request.method == 'POST':
        if request.data['termsSchoolMessage'] is not True:
            return Response(status.HTTP_400_BAD_REQUEST)
        log(1, "Message form school page", request)
        name = request.data['messageName']
        email = request.data['messageEmail']
        message = request.data['messageText']
        if validate_email(email) is not True:
            log(1, "Email invalid", email)
            return Response(status.HTTP_400_BAD_REQUEST)
        else:
            subject = "Mesaj de pe pagina Scolii de Dans de la " + email + " " + name
            listOfRecepients = [email]
            mail = MailSenderWrapper(subject, message, listOfRecepients)
            mail.send_email()
        return Response(status.HTTP_200_OK)
